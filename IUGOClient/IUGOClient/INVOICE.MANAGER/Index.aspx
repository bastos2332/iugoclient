﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="INVOICE.MANAGER.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:repeater runat="server" ID="rpt_faturas"></asp:repeater>
        <asp:Button runat="server" ID="btn_CriarFatura" Text="Criar Fatura" OnClick="btn_CriarFatura_Click" />
    </form>
</body>
</html>
