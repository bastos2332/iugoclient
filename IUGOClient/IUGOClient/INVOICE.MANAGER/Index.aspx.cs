﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace INVOICE.MANAGER
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CarregarFaturas();
        }

        protected void btn_CriarFatura_Click(object sender, EventArgs e)
        {
            CriarFatura();
        }

        private void CarregarFaturas()
        {
            CAD114_CONTA_IUGO ContaIUGO = new CAD114_CONTA_IUGO();
            ContaIUGO.IdConta = "14AD0D63F09D405691544803CE66A743";
            ContaIUGO.IdProvedor = 77;
            ContaIUGO.TokenTeste = "42f326864070df0e28ef44e034b5481c";
            ROOT Faturas = new FATURA_IUGO().GetAll(ContaIUGO);
        }

        private void CriarFatura()
        {
            CAD114_CONTA_IUGO ContaIUGO = new CAD114_CONTA_IUGO();
            ContaIUGO.IdConta = "14AD0D63F09D405691544803CE66A743";
            ContaIUGO.IdProvedor = 77;
            ContaIUGO.TokenTeste = "42f326864070df0e28ef44e034b5481c";

            Endereco endereco = new Endereco();
            endereco.Bairro = "Centro";
            endereco.Cep = "78134-000";
            endereco.Cidade = "Gabriel";
            endereco.Complemento = "Sala 909";
            endereco.Estado = "MT";
            endereco.Rua = "Miguel Sutil";
            endereco.Numero = 3380;
            endereco.Pais = "Brasil";

            Sacado sacado = new Sacado();
            sacado.NomeSacado = "Btor Solucoes Computacionais";
            sacado.Documento = "17365762000110";
            sacado.Email = "gabriel.bastos2332@gmail.com";
            sacado.Endereco = endereco;

            ITENS_FATURA itemFatura1 = new ITENS_FATURA();
            itemFatura1.DataAdicao = DateTime.Now;
            itemFatura1.DataAtualizacao = DateTime.Now;
            itemFatura1.Descricao = "Mensalidade Consulta Acerta PJ";
            itemFatura1.Quantidade = 1;
            itemFatura1.ValorCentavos = 5500;

            ITENS_FATURA itemFatura2 = new ITENS_FATURA();
            itemFatura1.DataAdicao = DateTime.Now;
            itemFatura1.DataAtualizacao = DateTime.Now;
            itemFatura1.Descricao = "Mensalidade Consulta Acerta PF";
            itemFatura1.Quantidade = 1;
            itemFatura1.ValorCentavos = 5500;

            ITENS_FATURA[] itens = new ITENS_FATURA[] {itemFatura1, itemFatura2 };
             
            FATURA_IUGO Fatura = new FATURA_IUGO();
            Fatura.EmailCobranca = "gabriel.bastos2332@gmail.com";
            Fatura.DataVencimento = DateTime.Now.AddMonths(2).ToShortDateString();
            Fatura.Sacado = sacado;
            Fatura.Itens = itens;
            Fatura.ValorTotalFaturaCentavos = 10500;

            FATURA_IUGO FaturaCriada = new FATURA_IUGO().Post(ContaIUGO, Fatura);
        }

    }
}