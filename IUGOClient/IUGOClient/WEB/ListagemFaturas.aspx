﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListagemFaturas.aspx.cs" Inherits="WEB.ListagemFaturas" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Faturas</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"/>
</head>
<body>
    <form id="form1" runat="server">
        <table class="table table-hover table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ID Fatura</th>
                    <th scope="col">Vencimento</th>
                    <th scope="col">Valor</th>
                    <th scope="col">Valor Corrigido</th>
                    <th scope="col">Cliente</th>
                    <th scope="col">Visualizar</th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater runat="server" ID="rpt_faturas">
                    <ItemTemplate>
                        <tr>
                            <th scope="row">
                                <asp:Literal runat="server" ID="lit_idFatura"></asp:Literal></th>
                            <td>
                                <asp:Literal runat="server" ID="lit_dataVencimento"></asp:Literal></td>
                            <td>
                                <asp:Literal runat="server" ID="lit_"></asp:Literal></td>

                            <td>
                                <asp:Literal runat="server" ID="lit_valorTitulo"></asp:Literal></td>

                            <td>
                                <asp:Literal runat="server" ID="lit_valorCorrigido"></asp:Literal></td>

                            <td>
                                <asp:Literal runat="server" ID="lit_nomeCliente"></asp:Literal></td>

                            <td>
                                <asp:LinkButton runat="server" ID="linkBtn_VisualizarFatura">Visualizar</asp:LinkButton></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </form>
</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</html>
