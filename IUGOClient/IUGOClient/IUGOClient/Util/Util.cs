﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

class Util
{
    public Uri GetURLBase()
    {
        return new Uri("https://api.iugu.com/v1/", UriKind.Absolute);
    }

    public string AsJsonList<T>(List<T> tt)
    {
        return new JavaScriptSerializer().Serialize(tt);
    }
    public string AsJson<T>(T t)
    {
        return new JavaScriptSerializer().Serialize(t);
    }
    public List<T> AsObjectList<T>(string tt)
    {
        return new JavaScriptSerializer().Deserialize<List<T>>(tt);
    }
    public T AsObject<T>(string t)
    {
        return new JavaScriptSerializer().Deserialize<T>(t);
    }
}

