﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class LOGS
{
    [JsonProperty("text")]
    public int Id { get; set; }
    [JsonProperty("text")]
    public string Descricao { get; set; }
    [JsonProperty("text")]
    public string Notas { get; set; }
    [JsonProperty("text")]
    public DateTime Data { get; set; }

}

