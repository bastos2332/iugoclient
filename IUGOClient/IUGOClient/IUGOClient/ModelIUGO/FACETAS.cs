﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class FACETAS
{
    [JsonProperty("late")]
    public LATE Late { get; set; }

    [JsonProperty("status")]
    public STATUS Status { get; set; }

}

