﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Sacado
{
    [JsonProperty("cpf_cnpj")]
    public string Documento { get; set; }
    [JsonProperty("name")]
    public string NomeSacado { get; set; }
    [JsonProperty("phone_prefix")]
    public string PrefixoTelefone { get; set; }
    [JsonProperty("phone")]
    public string Telefone { get; set; }

    [JsonProperty("email")]
    public string Email { get; set; }
    [JsonProperty("address")]
    public Endereco Endereco { get; set; }
}

