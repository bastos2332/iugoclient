﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

 
public class ROOT
{
    [JsonProperty("facets")]
    public FACETAS Facet { get; set; }

    [JsonProperty("totalItems")]
    public int TotalFaturas { get; set; }

    [JsonProperty("items")]
    public FATURA_IUGO[] Faturas { get; set; }
}

