﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public class BOLETO
{
    [JsonProperty("digitable_line")]
    public string LinhaDigitavel { get; set; }
    [JsonProperty("barcode_data")]
    public string CodigoDeBarras { get; set; }
    [JsonProperty("barcode")]
    public string URLCodigoDeBarras { get; set; }

}

