﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public class VARIAVEIS
{
    [JsonProperty("id")]
    public int Id { get; set; }
    [JsonProperty("variable")]
    public string ChaveVariavel { get; set; }
    [JsonProperty("value")]
    public string ValueVariavel { get; set; }

}

