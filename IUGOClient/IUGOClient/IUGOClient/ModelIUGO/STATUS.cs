﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class STATUS
{
    [JsonProperty("_type")]
    public string Type { get; set; }
    [JsonProperty("missing")]
    public int Missing { get; set; }
    [JsonProperty("other")]
    public int Other { get; set; }
    [JsonProperty("terms")]
    public TERMS[] Terms { get; set; }
    [JsonProperty("total")]
    public int Total { get; set; }
}

