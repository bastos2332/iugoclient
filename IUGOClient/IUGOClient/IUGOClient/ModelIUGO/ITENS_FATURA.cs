﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ITENS_FATURA
{
    [JsonProperty("id")]
    public string IdItem { get; set; }
    [JsonProperty("description")]
    public string Descricao { get; set; }
    [JsonProperty("price_cents")]
    public int ValorCentavos { get; set; }
    [JsonProperty("quantity")]
    public int Quantidade { get; set; }
    [JsonProperty("created_at")]
    public DateTime DataAdicao { get; set; }
    [JsonProperty("updated_at")]
    public DateTime DataAtualizacao { get; set; }
    [JsonProperty("price")]
    public string Valor { get; set; }
}

