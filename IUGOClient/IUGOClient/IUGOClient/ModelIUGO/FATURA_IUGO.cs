﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

public class FATURA_IUGO
{
    #region PROPS
    [JsonProperty("id")]
    public string IdFatura { get; set; }
    [JsonProperty("due_date")]
    public string DataVencimento { get; set; }
    [JsonProperty("currency")]
    public string Moeda { get; set; }
    [JsonProperty("discount_cents")]
    public string ValorDescontoCentavos { get; set; }
    [JsonProperty("email")]
    public string EmailCobranca { get; set; }
    //[JsonProperty("total_cents")]
    //public int ValorTotalItensCentavos { get; set; }
    [JsonProperty("notification_url")]
    public string URLNotificacao { get; set; }
    [JsonProperty("return_url")]
    public string URLRetorno { get; set; }
    [JsonProperty("status")]
    public string Status { get; set; }
    [JsonProperty("tax_cents")]
    public int? ValorTotalImpostoCentavos { get; set; }
    [JsonProperty("total_cents")]
    public int? ValorTotalFaturaCentavos { get; set; }
    [JsonProperty("total_paid_cents")]
    public int? TotalPagoCentavos { get; set; }
    [JsonProperty("taxes_paid_cents")]
    public int? ValorPagoImpostosCentavos { get; set; }
    [JsonProperty("paid_at")]
    public DateTime? DataPagamento { get; set; }
    [JsonProperty("paid_cents")]
    public int? ValorTotalPagoCentavos { get; set; }
    [JsonProperty("cc_emails")]
    public string EmailCC { get; set; }
    [JsonProperty("payable_with")]
    public string PagavelCom { get; set; }
    [JsonProperty("overpaid_cents")]
    public int? ValorPagoExecedidoCentavos { get; set; }
    [JsonProperty("ignore_due_email")]
    public string IgnorarEmail { get; set; }
    [JsonProperty("ignore_canceled_email")]
    public string IgnorarEmailCancelado { get; set; }
    [JsonProperty("advance_fee_cents")]
    public int? ValorAdiantamentoCentavos { get; set; }
    [JsonProperty("commission_cents")]
    public int? ValorComissaoCentavos { get; set; }
    //[JsonProperty("early_payment_discount")]
    //public Array[] ValorDescontoPagamentoAntecipado { get; set; }
    [JsonProperty("order_id")]
    public int? IdPedido { get; set; }
    [JsonProperty("updated_at")]
    public DateTime? DataAtualizacao { get; set; }
    [JsonProperty("secure_id")]
    public string IdFaturaSeguro { get; set; }
    [JsonProperty("secure_url")]
    public string URLFaturaSegura { get; set; }
    [JsonProperty("customer_id")]
    public int? IdCliente { get; set; }
    [JsonProperty("customer_ref")]
    public string ReferenciaDoCliente { get; set; }
    [JsonProperty("customer_name")]
    public string NomeCliente { get; set; }
    [JsonProperty("user_id")]
    public int? IdUsuario { get; set; }
    [JsonProperty("total")]
    public string Total { get; set; }
    [JsonProperty("taxes_paid")]
    public string ValorImpostosPagos { get; set; }
    [JsonProperty("total_paid")]
    public string ValorTotalPago { get; set; }
    [JsonProperty("total_overpaid")]
    public string ValorPagoExcesso { get; set; }
    [JsonProperty("commission")]
    public string ValorComissao { get; set; }
    [JsonProperty("fines_on_occurrence_day")]
    public string MultasDiaOcorrencia { get; set; }
    [JsonProperty("total_on_occurrence_day")]
    public string TotalNoDiaOcorrencia { get; set; }
    [JsonProperty("fines_on_occurrence_day_cents")]
    public int? ValotMultaDiaOcorrenciaCentavos { get; set; }
    [JsonProperty("total_on_occurrence_day_cents")]
    public int? ValorTotalDiaOcorrenciaDiaCentavos { get; set; }
    [JsonProperty("financial_return_date")]
    public DateTime? DataRetornoFinanceiro { get; set; }
    [JsonProperty("advance_fee")]
    public string TaxaAntecipada { get; set; }
    [JsonProperty("paid")]
    public string Pago { get; set; }
    //[JsonProperty("text")]
    //public int IdPagamentoOriginal { get; set; }
    //[JsonProperty("text")]
    //public int IdPagamentoDuplo { get; set; }
    //[JsonProperty("text")]
    //public string Interesse { get; set; }
    //[JsonProperty("text")]
    //public decimal Desconto { get; set; }
    //[JsonProperty("text")]
    //public DateTime DataCriacao { get; set; }
    //[JsonProperty("text")]
    //public string DataCriacaoISO { get; set; }
    //[JsonProperty("text")]
    //public DateTime DataAutorizacao { get; set; }
    //[JsonProperty("text")]
    //public DateTime DataAutorizacaoISO { get; set; }
    //[JsonProperty("text")]
    //public DateTime DataExpiracao { get; set; }
    //[JsonProperty("text")]
    //public DateTime DataExpiracaoISO { get; set; }
    //[JsonProperty("text")]
    //public DateTime DataDevolucao { get; set; }
    //[JsonProperty("text")]
    //public DateTime DataDevolucaoISO { get; set; }
    //[JsonProperty("text")]
    //public DateTime DataCancelamento { get; set; }
    //[JsonProperty("text")]
    //public DateTime DataCancelamentoISO { get; set; }
    //[JsonProperty("text")]
    //public DateTime DataProtesto { get; set; }
    //[JsonProperty("text")]
    //public DateTime DataProtestoISO { get; set; }
    //[JsonProperty("text")]
    //public DateTime DataEstorno { get; set; }
    //[JsonProperty("text")]
    //public DateTime DataEstornoISO { get; set; }
    //[JsonProperty("text")]
    //public DateTime DataOcorrencia { get; set; }
    //[JsonProperty("text")]
    //public DateTime DataOcorrenciaISO { get; set; }
    //[JsonProperty("text")]
    //public bool? FaturaReembolsavel { get; set; }
    //[JsonProperty("text")]
    //public int TotalParcelas { get; set; }
    //[JsonProperty("text")]
    //public int NumeroTransacao { get; set; }
    //[JsonProperty("text")]
    //public string MetodoPagamento { get; set; }
    //[JsonProperty("text")]
    //public DateTime[] DatasRetornoFinanceiro { get; set; }
    [JsonProperty("bank_slip")]
    public BOLETO Boleto { get; set; }
    [JsonProperty("items")]
    public ITENS_FATURA[] Itens { get; set; }
    //[JsonProperty("text")]
    //public decimal DescontoPagametoAntecipado { get; set; }
    [JsonProperty("variables")]
    public Sacado Sacado { get; set; }
    //[JsonProperty("text")]
    //public VARIAVEIS_CUSTOMIZADAS[] VariaveisCustomizadas { get; set; }
    //[JsonProperty("text")]
    //public LOGS[] Logs { get; set; }


    #endregion

    #region Obter Todas as faturas
    public ROOT GetAll(CAD114_CONTA_IUGO Conta)
    {
        var client = new RestClient("https://api.iugu.com/v1/invoices?api_token=" + Conta.TokenTeste);
        var request = new RestRequest(Method.GET);
        AdicionarCabecalho(ref request);
        IRestResponse response = client.Execute(request);
        JObject obj = JObject.Parse(response.Content);
        IList<object> FaturasObject = obj["items"].Select(t => (object)t).ToList();
        List<FATURA_IUGO> Faturas = new List<FATURA_IUGO>();
        for(int i = 0; i < Faturas.Count; i++)
        {
            FATURA_IUGO Fatura = new FATURA_IUGO();
            
            Faturas.Add(Fatura);
        }
        var result = JsonConvert.DeserializeObject<ROOT>(response.Content);
        return result;
    }
    #endregion

    #region Cancelar Fatura
    public FATURA_IUGO PutCancelFatura(CAD114_CONTA_IUGO Conta, string idFatura)
    {
        var client = new RestClient("https://api.iugu.com/v1/invoices/" + idFatura + "/cancel?api_token=" + Conta.TokenTeste);
        var request = new RestRequest(Method.PUT);
        AdicionarCabecalho(ref request);
        IRestResponse response = client.Execute(request);
        var result = JsonConvert.DeserializeObject<FATURA_IUGO>(response.Content);
        return result;
    }
    #endregion

    #region Obter Fatura Específica
    public FATURA_IUGO Get(CAD114_CONTA_IUGO Conta)
    {
        var client = new RestClient("https://api.iugu.com/v1/invoices/4AFDFBD884844BBEA5F98784D82CF1D2?api_token=" + Conta.TokenTeste);
        var request = new RestRequest(Method.GET);
        AdicionarCabecalho(ref request);
        IRestResponse response = client.Execute(request);
        var result = JsonConvert.DeserializeObject<FATURA_IUGO>(response.Content);
        return result;
    }

    #endregion

    #region Criar Fatura
    public FATURA_IUGO Post(CAD114_CONTA_IUGO Conta, FATURA_IUGO Fatura)
    {
        var client = new RestClient("https://api.iugu.com/v1/invoices?api_token=" + Conta.TokenTeste);
        var request = new RestRequest(Method.POST);
        AdicionarCabecalho(ref request);
      
        request.AddJsonBody(Fatura);

        IRestResponse response = client.Execute(request);
        var result = JsonConvert.DeserializeObject<FATURA_IUGO>(response.Content);
        if (response.IsSuccessful)
        {
            var teste = "teste";
        }
        return result;
    }

    #endregion

    public void AdicionarCabecalho(ref RestRequest request)
    {
        request.AddHeader("cache-control", "no-cache");
        request.AddHeader("Connection", "keep-alive");
        request.AddHeader("accept-encoding", "gzip, deflate");
        request.AddHeader("cookie", "__cfduid=d075cb3a6c58ce19c81717eb9389031da1560384945");
        request.AddHeader("Host", "api.iugu.com");
        request.AddHeader("Postman-Token", "7ca660c5-dc53-445d-a331-2e381824dc4e,98d295d5-37f1-4ec4-9c52-1fb3e59060bb");
        request.AddHeader("Cache-Control", "no-cache");
        request.AddHeader("Accept", "*/*");
        request.AddHeader("User-Agent", "PostmanRuntime/7.15.0");
    }

}

