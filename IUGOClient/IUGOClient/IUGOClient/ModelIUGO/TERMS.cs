﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class TERMS
{
    [JsonProperty("term")]
    public string Term { get; set; }
    [JsonProperty("count")]
    public int Count { get; set; }

}

