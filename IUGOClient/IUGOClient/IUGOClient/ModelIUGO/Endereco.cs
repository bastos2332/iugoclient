﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class Endereco
{
    [JsonProperty("zip_code")]
    public string Cep { get; set; }
    [JsonProperty("street")]
    public string Rua { get; set; }
    [JsonProperty("number")]
    public int Numero { get; set; }
    [JsonProperty("district")]
    public string Bairro { get; set; }
    [JsonProperty("city")]
    public string Cidade { get; set; }
    [JsonProperty("complement")]
    public string Complemento { get; set; }
    [JsonProperty("state")]
    public string Estado { get; set; }
    [JsonProperty("country")]
    public string Pais { get; set; }

}

