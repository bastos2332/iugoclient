﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public class CAD100_PROVEDOR
{
    public int idProvedor { get; set; }
    public string NomeProvedor { get; set; }
    public string NomeFantasia { get; set; }
    public string CNPJProvedor { get; set; }
    public int idStatusProvedor { get; set; }
    public string logoMarca { get; set; }
    public bool isEntidadeClasse { get; set; }
    public int idEnquadramento { get; set; }
    public DateTime dataInicioAtividade { get; set; }
    public int fusoHorario { get; set; }
    public int idCliente { get; set; }
    public int idEnumSytemProvider { get; set; }
    public int idProvedorLegado { get; set; }
    public string inscricaoEstadual { get; set; }
    public string inscricaoMunicipal { get; set; }
    public bool isAgendaPagos { get; set; }
    public bool isEstoqueCentral { get; set; }
    public bool isIntegrarEstoque { get; set; }
    public bool isProvedorMaster { get; set; }
    public bool isProvedorParceiro { get; set; }
    public bool isProvedorRegional { get; set; }
    public string nomeDivulgacao { get; set; }
    public string urlSite { get; set; }
    public int idCidade { get; set; }
    public string guid { get; set; }
    public bool isLancarAssociadoFornecedor { get; set; }
    public string presidente { get; set; }
    public string caminhoAssinatura { get; set; }
    public string tipoEntidade { get; set; }
    public int idProvedorPai { get; set; }
    public string codigoDiferencialRemessa { get; set; }
    public string tituloMensagemRetorno { get; set; }
    public string mensagemRetorno { get; set; }
    public bool isExporPrecoAssociacao { get; set; }
    public bool isExporPrecoAssociado { get; set; }
    public DateTime dataFundacao { get; set; }
    public DateTime dataDesfiliacao { get; set; }
    public int idRegional { get; set; }
    public string urlAplicacaoWEB { get; set; }
    public bool isBuscarBaixasSemSelicionarConta { get; set; }

}

