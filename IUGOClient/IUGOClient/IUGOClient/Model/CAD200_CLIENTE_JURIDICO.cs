﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class CAD200_CLIENTE_JURIDICO
{
    public int IdCliente { get; set; }
    public string razaoSocial { get; set; }
    public string NomeFantasia { get; set; }
    public string inscricaoEstadual { get; set; }
    public DateTime dataFundacao { get; set; }
    public string CNPJ { get; set; }
    public DateTime dataCadastro { get; set; }
    public string NIRE { get; set; }
    public bool isSimplesNacional { get; set; }
    public bool isMEI { get; set; }
    public bool isEIRELI { get; set; }
    public int idNaturezaJuridica { get; set; }
    public DateTime dataUltimaAtualizacaoReceita { get; set; }
    public string situacaoReceita { get; set; }
    public int idPorte { get; set; }
}