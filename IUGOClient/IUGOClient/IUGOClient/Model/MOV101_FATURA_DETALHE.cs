﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class MOV101_FATURA_DETALHE
{
    public int idFatura { get; set; }
    public int idLancamento { get; set; }
    public int QTD { get; set; }
    public decimal ValorUnitario { get; set; }
    public bool isInvertido { get; set; }
    public decimal RateioPerCent { get; set; }
    public int idProduto { get; set; }
    public string Historico { get; set; }
    public bool isCompoeFatura { get; set; }
    public bool isContratoConsumacao { get; set; }
    public bool isContratoMensalidade { get; set; }
    public bool isMensalidadeFixa { get; set; }
    public int idClienteOrigem { get; set; }
    public int idFaturaOrigem { get; set; }
    public int idDetalheFatura { get; set; }


}

