﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public class MOV101_FATURA
{
    public int idFatura { get; set; }
    public DateTime dataLancamento { get; set; }
    public int idCliente { get; set; }
    public DateTime dataVencimento { get; set; }
    public int idTipoDocumentoCobranca { get; set; }
    public decimal valorTotal { get; set; }
    public int numeroDocumentoCobranca { get; set; }
    public int idStatusFatura { get; set; }
    public int idContaCorrente { get; set; }
    public string nossoNumero { get; set; }
    public decimal valorPagoMomento { get; set; }
    public decimal valorDesconto { get; set; }
    public bool isEnviarDetalhe { get; set; }
    public int idVencimentoFatura { get; set; }
    public string chaveBoletoWeb { get; set; }
    public bool isProrrogada { get; set; }
    public bool isIncluida { get; set; }
    public bool isOrigemRenegociacao { get; set; }
    public bool isConsolidado { get; set; }
    public DateTime dataEmCobranca { get; set; }
    public int idUsuarioCobranca { get; set; }
    public bool isEmCobranca { get; set; }
    public bool isProrrogadaIndividualmente { get; set; }
    public int idClienteDesvioProducao { get; set; }
    public int idProvedor { get; set; }
    public int idRegional { get; set; }
    public bool isPreFaturado { get; set; }
    public int proximoNossoNumero { get; set; }
    public decimal valorTotalJuros { get; set; }
    public decimal valorTotalMulta { get; set; }
    public int idRegistro { get; set; }
    public bool isAcumulaJurosMulta { get; set; }
    public bool isEnviadoRemessa { get; set; }
    public string codigoOcorrenciaAtual { get; set; }
    public int idVenda { get; set; }
}
