﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class LKP200_PRODUTO
{
    public int idProduto { get; set; }
    public string nomeProduto { get; set; }
    public bool isEstoque { get; set; }
    public bool isVisivel { get; set; }
    public bool isComposicao { get; set; }
    public string textoPublicitario { get; set; }
    public bool isEComm { get; set; }
    public bool isInclusaoRegistro { get; set; }
    public bool isExclusaoRegistro { get; set; }
    public string nomeIntegracaoFaturamento { get; set; }
    public bool isFaturar { get; set; }
    public bool isMensalidade { get; set; }
    public bool isContratoConsumacao { get; set; }
    public bool isContratoMensalidade { get; set; }
    public int idOrigemLancamento { get; set; }
    public int idTipoLancamentoReceita { get; set; }
    public int idCategoriaProduto { get; set; }
    public string codigoProduto { get; set; }
    public bool isProdutoParaDesconto { get; set; }
    public bool isProdutoParAjuste { get; set; }
    public bool isSubProduto { get; set; }
    public bool isVigorarComissaoConsultor { get; set; }
    public bool isVisivelVenda { get; set; }
    public int idProvedor { get; set; }
    public DateTime dataEntradaPortfolio { get; set; }
    public int idGrupoContas { get; set; }
    public int idSubGrupoConta { get; set; }
    public string imagem { get; set; }
    public bool isEstoqueVirtual { get; set; }
    public bool isProdutoMaster { get; set; }
    public string nomeDivulgacao { get; set; }
    public string numeroCodigoBarra { get; set; }
    public string unidadeValidade { get; set; }
    public string urlProduto { get; set; }
    public int validade { get; set; }
    public string classeCorCss { get; set; }
    public int idFamilia { get; set; }
    public int idProdutoLegado { get; set; }
    public int idSubCategoriaProduto { get; set; }
    public bool isAgrupaPlanos { get; set; }
    public bool isPlano { get; set; }
    public bool isVisivelEntrega { get; set; }
    public string statusProduto { get; set; }
    public bool isCarta { get; set; }
    public int idEvento { get; set; }
    public int idCentroCusto { get; set; }
    public string modalidade { get; set; }
    public bool isProdutoPrincipal { get; set; }
    public string modalidadeProduto { get; set; }
    public bool isConsultaSistemaTela { get; set; }
    public bool isFeatureConsultaSistemaTela { get; set; }
    public string chaveIdentificacaoProduto { get; set; }
    public bool isProdutoParaDegustacao { get; set; }
    public bool isPermiteAlterarValorVendaBalcao { get; set; }
    public bool isMensalidadeProducao { get; set; }

}
