﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public class CAD200_CLIENTE
{
    public int idCliente { get; set; }
    public int idProvedor { get; set; }
    public bool isProvedor { get; set; }
    public bool isECliente { get; set; }
    public bool logoMarca { get; set; }
    public int idSituacaoCliente { get; set; }
    public string tipoPessoa { get; set; }
    public bool isEnviadoProRede { get; set; }
    public bool isSucessoProRede { get; set; }
    public bool isErroProRede { get; set; }
    public bool isEmiteNota { get; set; }
    public bool isImpostoRetido { get; set; }
    public DateTime dataCadastro { get; set; }
    public bool isEnvioManualProRede { get; set; }
    public bool isReceberDetalheFatura { get; set; }
    public DateTime dataUltimoBloqueio { get; set; }
    public string Observacoes { get; set; }
    public string emailCobranca { get; set; }
    public DateTime dataInicialFaturamento { get; set; }
    public DateTime dataFinalFaturamento { get; set; }
    public int idRegional { get; set; }
    public bool isPossuiRegistroInadimplencia { get; set; }
    public int idEmpresaMatriz { get; set; }
    public bool isEmpresaFilial { get; set; }
    public int idClienteLegado { get; set; }
    public bool isPermiteRequisicao { get; set; }
    public DateTime dataVencimentoPadrao { get; set; }
    public bool isNaoAssociado { get; set; }
    public string mapa { get; set; }
    public string site { get; set; }
    public bool IsPossuiLog { get; set; }
    public bool isVisivelFederacao { get; set; }
    public int idGrupoEmpresa { get; set; }
    public bool isContratoAssinado { get; set; }

}

