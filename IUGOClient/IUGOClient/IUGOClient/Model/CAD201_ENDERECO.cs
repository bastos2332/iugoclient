﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class CAD201_ENDERECO
{
    public int IdCliente { get; set; }
    public int idTipoEndereco { get; set; }
    public int idLogradouro { get; set; }
    public string Endereco { get; set; }
    public string Bairro { get; set; }
    public int idCidade { get; set; }
    public string UF { get; set; }
    public string CEP { get; set; }
    public string Complemento { get; set; }
    public string Numero { get; set; }
    public bool isEnderecoCobranca { get; set; }

}
