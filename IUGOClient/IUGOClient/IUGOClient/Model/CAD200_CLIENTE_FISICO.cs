﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class CAD200_CLIENTE_FISICO
{
    public int IdCliente { get; set; }
    public string nome { get; set; }
    public string RG { get; set; }
    public DateTime dataEmissao { get; set; }
    public string orgaoEmissor { get; set; }
    public DateTime dataNascimento { get; set; }
    public string CPF { get; set; }
    public DateTime dataCadastro { get; set; }
    public int idCBO { get; set; }
    public string outroDocumento { get; set; }

}

